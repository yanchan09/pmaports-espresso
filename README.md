postmarketOS for espresso10 (Galaxy Tab 2 10.1), now with mainline kernel

## Features
|     |     |
| --- | --- |
| Display | Works, although there's some color artifacting in phosh |
| USB | Mostly works |
| Touchscreen | Mostly works |
| Accelerometer | Wrong orientation |
| Wifi | Works |
| Charging | Should work |
| Sound | Broken |
| Buttons | Vol-down doesn't work |
| 3D acceleration | Broken |
